## Instructions
* Write a web application in Java which meets the following requirements:
 * Allow users to enter a string which will be encrypted or decrypted by a selected algorithm
 * Implement encryption to Morse code and decryption from Morse code
 * Your design and solution should allow an easy extension of the application by adding another encryption and decryption algorithms (a Caesar cipher for example)
* Show your programming skills as well as your problem-solving skills. A clean and readable design and code and adhering to the KISS principle is much more important than applying artificially all the design patterns you know.

## Solution
I made RestAPI application. Maybe it would be more simpler, but i tried to made extensiable application so these are parts of application

### cz.sachadaniel.homework.controller.CodingController 
Controller thats receive demand and call service, that make output.

### codingService
It is object of interface ICodingService, so it can have adjustable implementation (see BeanConfig).
Actually implementation is BaseCodingService, that can encrypt/decrypt Morse code (by morseCoding). Morse coding is so object of interface and it can be implemented by another way.
Program can have next coding by this procedure:
* create new class into package cz.sachadaniel.homework.service.coding, that implements interface ICoding
* add new attribute into BaseCodingService like morseCoding
* add conditions into method coding of BaseCodingService
* create bean with new coding into BeansConfig

### morseCoding
It is implementation of interface ICoding and it encrypt/decrypt Morse code. Implementation is not optimized because of time reasons.

### beansConfig
It created beans for service and coders

### Application
It runs application.

### Run server:
- move to work dir
- mvn clean;mvn spring-boot:run

### Run tests
- move to work dir
- java -jar ~/.m2/repository/org/junit/platform/junit-platform-console-standalone/1.0.0-M4/junit-platform-console-standalone-1.0.0-M4.jar --class-path target/classes --class-path target/test-classes --scan-class-path

### Run decrypt from command line
- curl -X POST http://localhost:8080/api/coding -H 'Cache-Control: no-cache' -H 'Content-Type: application/json' -d '{"source": "plain", "destination":"morse", "text": "ahojnazdarcau"}'

### Run encrypt from command line
- curl -X POST http://localhost:8080/api/coding -H 'Cache-Control: no-cache' -H 'Content-Type: application/json' -d '{"source": "morse", "destination":"plain", "text": ".- .... --- .--- -. .- --.. -.. .- .-. -.-. .- ..-"}'
