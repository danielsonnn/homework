package cz.sachadaniel.homework.config;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import cz.sachadaniel.homework.service.BaseCodingService;
import cz.sachadaniel.homework.service.ICodingService;
import cz.sachadaniel.homework.service.coding.ICoding;
import cz.sachadaniel.homework.service.coding.MorseCoding;

/**
 * Configuration for application
 * If you want to add new coding you can do:
 * - create new class into package cz.sachadaniel.homework.service.coding, that implements interface ICoding
 * - add new attribute into BaseCodingService like morseCoding
 * - add conditions into method coding of BaseCodingService
 * - create bean with new coding into BeansConfig  
 * 
 * @author dan
 *
 */
@Configuration
@EnableAutoConfiguration
@EnableWebMvc
@ComponentScan("cz.sachadaniel.homework")
public class BeansConfig {

	/**
	 * Service that process coding
	 * @return
	 */
	@Bean
	public ICodingService codingService() {
		return new BaseCodingService();
	}

	/**
	 * Object for Morse coding
	 * @return
	 */
	@Bean
	public ICoding morseCoding() {
		return new MorseCoding();
	}

}
