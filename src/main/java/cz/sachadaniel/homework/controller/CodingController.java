package cz.sachadaniel.homework.controller;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Assert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import cz.sachadaniel.homework.service.ICodingService;
import cz.sachadaniel.homework.service.coding.CodingCrate;

@Controller
public class CodingController {

	private static Logger LOGGER = LogManager.getLogger(CodingController.class);
	
	@Autowired
	private ICodingService codingService;

    @GetMapping("/")
    public String redirect() {
        return "redirect:/coding";
    }

    @GetMapping("/coding")
    public String codingForm(Model model) {

        model.addAttribute("coding", new CodingCrate());
        
        List<String> codings = new ArrayList<>();
        codings.add("plain");
        codings.add("morse");
        
        model.addAttribute("codings", codings);
        
        return "coding";
    }

    @PostMapping("/coding")
    public String codingSubmit(@ModelAttribute CodingCrate codingCrate) {
    	
		Assert.assertNotNull(this.codingService);
		String encrypt = this.codingService.coding(codingCrate);
		codingCrate.setResult(encrypt);

        return "result";
    }

}
