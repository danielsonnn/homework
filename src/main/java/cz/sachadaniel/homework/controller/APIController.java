package cz.sachadaniel.homework.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Root of all controllers
 * 
 * @author dan
 *
 */
@RestController
@RequestMapping(APIController.API_ROOT)
public class APIController {

	public static final String API_ROOT = "/api";

}
