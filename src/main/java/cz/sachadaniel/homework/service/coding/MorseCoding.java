package cz.sachadaniel.homework.service.coding;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Class for encrypt/decrypt data for Morse coding
 * @author dan
 *
 */
public class MorseCoding implements ICoding {

	private static Logger LOGGER = LogManager.getLogger(MorseCoding.class);

	private String[] english = { "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q",
			"r", "s", "t", "u", "v", "w", "x", "y", "z", "1", "2", "3", "4", "5", "6", "7", "8", "9", "0", ",", ".",
			"?" };

	private String[] morse = { ".-", "-...", "-.-.", "-..", ".", "..-.", "--.", "....", "..", ".---", "-.-", ".-..",
			"--", "-.", "---", ".---.", "--.-", ".-.", "...", "-", "..-", "...-", ".--", "-..-", "-.--", "--..",
			".----", "..---", "...--", "....-", ".....", "-....", "--...", "---..", "----.", "-----", "--..--",
			".-.-.-", "..--.." };

	/**
	 * Encrypt data
	 * 
	 * @param data input data
	 * @return encrypted data
	 */
	public String encrypt(String data) {
		
		if (data == null) {
			return null;
		}
		
		String ret = "";
		char[] chars = data.toLowerCase().toCharArray();
		
		for (int i = 0; i < chars.length; i++) {
			boolean find = false;
			
			for (int j = 0; j < english.length; j++) {
				if (chars[i] == english[j].charAt(0)) {
					ret += ((ret.length() > 0) ? " " : "") + morse[j];
					find = true;
					continue;
				}
			}
			
			if (!find) {
				throw new IllegalArgumentException("Bad char '" + chars[i] + "' in " + data);
			}

		}
		
		return ret;
	}

	/**
	 * Decrypt data
	 * 
	 * @param data input data
	 * @return decrypted data
	 */
	public String decrypt(String data) {
		
		if (data == null) {
			return null;
		}

		int position = 0;
		String ret = "";
		
		while (position < data.length()) {
			boolean found = false;
			
			for (int j = 0; j < morse.length; j++) {
				
				if (data.equals(morse[j])) {
					ret += english[j];
					return ret;
				}
				
				if (data.startsWith(morse[j] + " ")) {
					int mov = morse[j].length() + 1;
					ret += english[j];
					data = data.substring(mov);
					position = 0;
					found = true;
					break;
				}
				
			}
			
			if (!found) {
				throw new IllegalArgumentException("Bad morse code=" + data + ".");
			}
			
		}
		
		throw new IllegalArgumentException("Bad morse code=" + data + ".");
		
	}
}
