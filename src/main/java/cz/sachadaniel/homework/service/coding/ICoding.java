package cz.sachadaniel.homework.service.coding;

/**
 * Interface for encrypt and decrypt data
 * @author dan
 *
 */
public interface ICoding {

	/**
	 * Encrypt data
	 * 
	 * @param data input data
	 * @return encrypted data
	 */
	String encrypt(String data);
	
	/**
	 * Decrypt data
	 * 
	 * @param data input data
	 * @return decrypted data
	 */
	String decrypt(String data);

}
