package cz.sachadaniel.homework.service.coding;

/**
 * Crate for coding and text
 * @author dan
 *
 */
public class CodingCrate {

	/**
	 * Source format of text
	 */
	private String source;
	
	/**
	 * Destination format of text
	 */
	private String destination;
	
	/**
	 * Text for coding
	 */
	private String text;

	/**
	 * Encoded text
	 */
	private String result;

	public CodingCrate() {
	}
	
	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getDestination() {
		return destination;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

}
