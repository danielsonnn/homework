package cz.sachadaniel.homework.service;

import cz.sachadaniel.homework.service.coding.CodingCrate;

/**
 * Interface for coding
 */
public interface ICodingService {

	/**
	 * Encrypt/decrypt data for supported coding
	 * 
	 * @param codingCrate object with coding and text
	 * @throws IllegalArgumentException if combination for coding is not supported
	 */
	public String coding(CodingCrate codingCrate) throws IllegalArgumentException;

}
