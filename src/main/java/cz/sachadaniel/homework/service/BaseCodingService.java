package cz.sachadaniel.homework.service;

import org.junit.Assert;
import org.springframework.beans.factory.annotation.Autowired;

import cz.sachadaniel.homework.service.coding.CodingCrate;
import cz.sachadaniel.homework.service.coding.ICoding;

/**
 * Encrypt/decrypt data for known coding
 * 
 * @author dan
 *
 */
public class BaseCodingService implements ICodingService {

	/**
	 * Object for Morse coding
	 */
	@Autowired
	private ICoding morseCoding;

	/**
	 * Encrypt/decrypt data for supported coding
	 * 
	 * @param codingCrate object with coding and text
	 * @throws IllegalArgumentException if combination for coding is not supported
	 */
	@Override
	public String coding(CodingCrate codingCrate) throws IllegalArgumentException {

		Assert.assertNotNull(codingCrate);
		
		if (codingCrate.getSource().equals("plain") && codingCrate.getDestination().equals("morse")) {
			Assert.assertNotNull(this.morseCoding);
			return this.morseCoding.encrypt(codingCrate.getText());
		}
		
		if (codingCrate.getSource().equals("morse") && codingCrate.getDestination().equals("plain")) {
			Assert.assertNotNull(this.morseCoding);
			return this.morseCoding.decrypt(codingCrate.getText());
		}
		
		throw new IllegalArgumentException("Source " + codingCrate.getSource() + " and destination "
				+ codingCrate.getDestination() + " is not supported");
		
	}

}
