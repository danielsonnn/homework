package cz.sachadaniel.homework;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Application {

	private static Logger LOGGER = LogManager.getLogger(Application.class);

	public static void main(String[] args) {

		LOGGER.debug("Running application!");
		SpringApplication.run(Application.class, args);

	}
}
