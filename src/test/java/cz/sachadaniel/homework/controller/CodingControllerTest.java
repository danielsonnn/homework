package cz.sachadaniel.homework.controller;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.jayway.restassured.RestAssured;
import com.jayway.restassured.response.Response;

class CodingControllerTest {
	
	private static final String path="/api/coding";
	private static final String contentType="application/json";

	@BeforeAll
	static void setUpBeforeClass() throws Exception {
	}

	@AfterAll
	static void tearDownAfterClass() throws Exception {
	}

	@BeforeEach
	void setUp() throws Exception {
    	RestAssured.baseURI  = "http://127.0.0.1:8080";	
	}

	@AfterEach
	void tearDown() throws Exception {
	}

	/**
	 * Good test for encrypt plain text to Morse code 
	 */
	@Test
	void plainToMorseTest_True() {

    	Response r = RestAssured.given()
    	.contentType(this.contentType).
    	body("{\"source\":\"plain\", \"destination\":\"morse\", \"text\":\"ahojnazdarcau\"}").
        when().
        post(this.path);

    	String body = r.getBody().asString();
    	assertTrue(body.equals(".- .... --- .--- -. .- --.. -.. .- .-. -.-. .- ..-"));
	}

	/**
	 * Good test for encrypt plain text containing big letters to Morse code 
	 */
	@Test
	void plainToMorseTest_BigLettersTrue() {

    	Response r = RestAssured.given()
    	.contentType(this.contentType).
    	body("{\"source\":\"plain\", \"destination\":\"morse\", \"text\":\"ahoJnaZdarcau\"}").
        when().
        post(this.path);

    	String body = r.getBody().asString();
    	assertTrue(body.equals(".- .... --- .--- -. .- --.. -.. .- .-. -.-. .- ..-"));
	}

	/**
	 * Bad test for encrypt plain text to Morse code (invalid letters ss) 
	 */
	@Test
	void plainToMorseTest_False() {

    	Response r = RestAssured.given()
    	.contentType(this.contentType).
    	body("{\"source\":\"plain\", \"destination\":\"morse\", \"text\":\"ahojddzdarcau\"}").
        when().
        post(this.path);

    	String body = r.getBody().asString();
    	assertFalse(body.equals(".- .... --- .--- -. .- --.. -.. .- .-. -.-. .- ..-"));
	}

	/**
	 * Good test for decrypt Morse code back to plain text  
	 */
	@Test
	void morseToPlainTest_True() {

    	Response r = RestAssured.given()
    	.contentType(this.contentType).
    	body("{\"source\":\"morse\", \"destination\":\"plain\", \"text\":\".- .... --- .--- -. .- --.. -.. .- .-. -.-. .- ..-\"}").
        when().
        post(this.path);

    	String body = r.getBody().asString();
    	assertTrue(body.equals("ahojnazdarcau"));
	}

	/**
	 * Bad test for decrypt Morse code back to plain text (invalid letters dd)
	 */
	@Test
	void morseToPlainTest_False() {

    	Response r = RestAssured.given()
    	.contentType(this.contentType).
    	body("{\"source\":\"morse\", \"destination\":\"plain\", \"text\":\".- .... --- .--- -. .- --.. -.. .- .-. ss-.-. .- ..-\"}").
        when().
        post(this.path);

    	String body = r.getBody().asString();
    	assertFalse(body.equals("ahojnazdarcau"));
	}

}
